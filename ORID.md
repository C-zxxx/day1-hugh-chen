**O**: Today's learning content  
* ① Concept Map:  
    >Key words: concept, link, link words, propositions, focus question.  
Steps:Determine focus question, Identify key concept, Rank order cpncept, Connect concept, Modify structure, Add example  
Usage: Visual thinking, Knowledge Connection, Summarize  
* ② PDCA:  
    >Plan-Doing-Check-Adjust  
* ③ Stand Meeting: 
    >Reduce blocks  
* ④ ORID: 
    >Key words: Objective, Reflective, Interpretive, Decisional  
***
**R**: Benefiting greatly  
***
**I**: I have gained a deep understanding of many unfamiliar knowledge and deepened communication with team members.  
***
**D**: In future development and learning, continue to practice Concept Map, PDCA, ORID and other knowledge to deepen impression and understanding.